BASE_DIR="./"
LIB_FOLDER="$BASE_DIR/dependency"
MAIN_JAR_FILE="$BASE_DIR/ad-parser-1.0.jar"

# Running mode
# 1: enable; 0: disable
allSubFolder="0"

# Interval sleeping
intervalSleeping="5000"

# Parser
RAW_LOG_FOLDER="/Users/youtech/Desktop/FTI/sampleLog/day=2014-12-30/hour=14"
NUMBER_OF_URLs="100"

# Executor
corePoolSize="1"
maxPoolSize="5"
numberOfTasksInQueue="1000"

# Redis
hostIp="127.0.0.1"
port="6379"


function createConfigFile() {	
	rm -f $BASE_DIR/src/main/resources/configParser.properties
	touch $BASE_DIR/src/main/resources/configParser.properties
	
	echo "allSubFolder=$allSubFolder" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "intervalSleeping=$intervalSleeping" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "RAW_LOG_FOLDER=$RAW_LOG_FOLDER" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "NUMBER_OF_URLs=$NUMBER_OF_URLs" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "corePoolSize=$corePoolSize" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "maxPoolSize=$maxPoolSize" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "numberOfTasksInQueue=$numberOfTasksInQueue" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "hostIp=$hostIp" >> $BASE_DIR/src/main/resources/configParser.properties
	echo "port=$port" >> $BASE_DIR/src/main/resources/configParser.properties
}

function main() {
	export ANTS_DETECT_TOPIC_RESOURCES=/Users/youtech/Documents/repos/ants-dm-rad/resources
	for file in $LIB_FOLDER/*.jar
	do
		CLASSPATH=$file:$CLASSPATH
	done

	CLASSPATH=$CLASSPATH:$MAIN_JAR_FILE

	java "-Dlog4j.configuration=file:./src/main/resources/log4j.xml" -cp $CLASSPATH fpt.innovation.adParser.App
}

createConfigFile
main


